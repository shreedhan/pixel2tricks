#!/bin/bash
resolution_already_added=true;
if ! xrandr|grep -q "1920x1275"; then
	resolution_already_added=false;
	cvt 2160 1440 60
	xrandr --newmode "2160x1440"  263.50  2160 2320 2552 2944  1440 1443 1453 1493 -hsync +vsync
	xrandr --addmode eDP1 2160x1440

	cvt 1920 1275 60
	xrandr --newmode "1920x1275"  205.50  1920 2056 2256 2592  1275 1278 1288 1322 -hsync +vsync
	xrandr --addmode eDP1 1920x1275

	cvt 1600 1062 60
	xrandr --newmode "1600x1062"  141.50  1600 1704 1872 2144  1062 1065 1075 1102 -hsync +vsync
	xrandr --addmode eDP1 1600x1062

	cvt 1440 956 60
	xrandr --newmode "1440x956"  113.25  1440 1528 1672 1904  956 959 969 992 -hsync +vsync
	xrandr --addmode eDP1 1440x956

	cvt 1280 850 60
	xrandr --newmode "1280x850"   88.75  1280 1352 1480 1680  850 853 863 883 -hsync +vsync
	xrandr --addmode eDP1 1280x850

	cvt 1024 680 60
	xrandr --newmode "1024x680"   56.25  1024 1072 1176 1328  680 683 693 707 -hsync +vsync
	xrandr --addmode eDP1 1024x680
fi

filepath="$HOME/.resolution"

if [[ -f $filepath && $resolution_already_added ]]; then
	resolution=$(cat $filepath)
else
	resolution="2560x1700"
fi

case $resolution in
	"2560x1700")
	resolution="2160x1440"
	;;
	"2160x1440")
	resolution="1920x1275"
	;;
	"1920x1275")
	resolution="1600x1062"
	;;
	"1600x1062")
	resolution="1440x956"
	;;
	"1440x956")
	resolution="1280x850"
	;;
	"1280x850")
	resolution="1024x680"
	;;
	"1024x680")
	resolution="2560x1700"
	;;
esac

xrandr --output eDP1 --mode $resolution

echo $resolution > $filepath
